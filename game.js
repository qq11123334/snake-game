const canvas = document.getElementById("canvas");
const canvasContext = canvas.getContext("2d");
const gridSize = 20;
const boundaryX = canvas.width / gridSize;
const boundaryY = canvas.height / gridSize;


window.onload = () => {
	apple.generate();
	gameLoop();
}

function gameLoop() {
	setInterval(update, 1000 / 15);
}

function update() {
	canvasContext.clearRect(0, 0, canvas.width, canvas.height);
	snake.move();
	EatApple();
	draw();
}

function Rect(x, y, width, height, color) {
	canvasContext.fillStyle = color;
	canvasContext.fillRect(x, y, width, height);
}

function draw() {
	Rect(0, 0, canvas.width, canvas.height, "black");
	Rect(apple.x * gridSize + 2.5, apple.y * gridSize + 2.5, 
		gridSize - 5, gridSize - 5, "red");
	for(var i = 0; i < snake.tail.length; i++) {
		if(i == snake.tail.length - 1) color = "green"
		else color = "white"
		Rect(snake.tail[i].x * gridSize + 2.5, snake.tail[i].y * gridSize + 2.5, 
			gridSize - 5, gridSize - 5, color);
	}
}

window.addEventListener("keydown", function (event) {
	if(event.key == "ArrowLeft") {
		snake.directionX = -1;
		snake.directionY = 0;
	} else if(event.key == "ArrowRight") {
		snake.directionX = 1;
		snake.directionY = 0;
	} else if(event.key == "ArrowUp") {
		snake.directionX = 0;
		snake.directionY = -1;
	} else if(event.key == "ArrowDown") {
		snake.directionX = 0;
		snake.directionY = 1;
	}
});

class Apple {
	generate() {
		while(true) {
			this.x = Math.floor(Math.random() * boundaryX)
			this.y = Math.floor(Math.random() * boundaryY)
			
			let isTouching = false;
			for(let i = 0; i < snake.tail.length; i++) {
				let body = snake.tail[i];
				if(body.x == this.x && body.y == this.y) {
					isTouching = true;
				}
			}

			if(!isTouching) {
				break;
			}
		}
	}
}

function EatApple() {
	let head = snake.tail[snake.tail.length - 1];
	if(apple.x == head.x && apple.y == head.y) {
		apple.generate();
		snake.tailLength++;
	}
}

class Snake {
	constructor(x, y) {
		this.x = x;
		this.y = y;
		this.tailLength = 1;
		this.tail = [{x: this.x, y:this.y}];
		this.directionX = 0;
		this.directionY = 0;
	}
	
	move() {
		this.x += this.directionX;
		this.y += this.directionY;
		this.x = (this.x + boundaryX) % boundaryX;
		this.y = (this.y + boundaryY) % boundaryY;
		this.tail.push({x: this.x, y:this.y});
		if(this.tail.length > this.tailLength) {
			this.tail.shift();
		}
	}
}

const snake = new Snake(boundaryX / 2, boundaryY / 2);
const apple = new Apple();