var http = require('http');
var fs = require('fs');
var index = 'index.html';
var gamejs = 'game.js';

http.createServer(function (request, response) {
	console.log(request.url);
	response.writeHead(200, {'Content-Type': 'html'});
	var html = fs.readFileSync(index, 'utf8');
	var js = fs.readFileSync(gamejs, 'utf8');
	if(request.url == "/game.js") response.write(js); 
	if(request.url == "/") response.write(html); 
	response.end();
}).listen(8080);